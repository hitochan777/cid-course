# 京都工芸繊維大学 情報工学専攻 インタラクションデザイン学コース #

京都工芸繊維大学　情報工学専攻　インタラクションデザイン学コースのウェブサイトのソースコードです。

# System Requirements
* [Bower](http://bower.io/)(パッケージ管理ソフト)
* mod_rewriteが有効化されていること
        
        sudo a2enmod rewrite
        sudo /etc/init.d/apache2 restart

# インストール方法

	cd cid-course
	bower install

.htaccessのRewriteBase及びindex.htmlのbaseタグのhrefをドキュメントルートに変更してください。これらが適切に設定されていない場合、CSSやJavascriptが適応されません。

# 公開講義「認知的インタラクションデザイン学」の今後の予定とアーカイブの内容
公開講義「認知的インタラクションデザイン学」の今後の予定とアーカイブの内容はjson/talks.jsonにjson形式で書かれています。

スライドは西暦ごと、日程ごとにまとめられています。日程とは、日時と場所が同じ状態を示します。　同じ日程で複数のスライドがある場合があるかと思います。その場合は複数のセクションを指定してください。

# TODO
