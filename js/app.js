var app = angular.module("app",[
		"ngAnimate",
		"ngRoute",
        "ngSanitize",
		"controllers"
]);

app.config(['$routeProvider','$locationProvider',
		function($routeProvider,$locationProvider) {
			$locationProvider.html5Mode(true);
			$routeProvider.
				when('/', {
					templateUrl: 'views/outline.html'
				}).
				when('/talks', {
					templateUrl: 'views/talks.html',
					controller: 'talkListCtrl'
				}).
				when('/cid', {
					templateUrl: 'views/cid.html'
				}).
				otherwise({
					templateUrl: 'views/notfound.html'
				});	
		}
]
);

app.filter('trustAsResourceUrl', ['$sce', function($sce) {
	return function(val) {
		return $sce.trustAsResourceUrl(val);
	};
}]);

app.filter('trustHtml', ['$sce',function($sce) {
	return $sce.trustAsHtml;
}]);
