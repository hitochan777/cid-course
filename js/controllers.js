var controllers = angular.module('controllers', []);

controllers.controller("talkListCtrl",["$scope","$http","$routeParams","$location",function($scope,$http,$routeParams,$location){
	$scope.talks = [];
	if($location.hash()){
		$scope.currentYear=$location.hash();
	}
	else{
		$location.hash((new Date()).getFullYear());
		$scope.currentYear = (new Date()).getFullYear();
	}
	$http.get("json/talks.json").success(function(data){
		$scope.talks = data;
		$scope.years = Object.keys(data);
		$scope.talkNum = {};
		/***********************Adding counter to the speakers of each talk*********************************/
		angular.forEach($scope.talks,function(yearTalks,index){
			$scope.talkNum[index] = 0;
			angular.forEach(yearTalks.collections, function(collection, index1){
				if("seminar" in collection){
					angular.forEach(collection.seminar, function(talk, index2){
						var counter = 1;
						angular.forEach(talk.speakers, function(speaker, index3){
							speaker.counter = counter++;
						});
						$scope.talkNum[index]++;
					});
				}
				else{
					var counter = 1;
					angular.forEach(collection.speakers, function(speaker, index2){
						speaker.counter = counter++;
					});
					$scope.talkNum[index]++;
				}
			})
		});
		/********************************************************************************/
		$scope.numberOfPages=function(){
			return Math.ceil($scope.talkNum[$scope.currentYear]/$scope.pageSize);
		}
		$scope.sliced = [];
		for(var p = 0; p < $scope.numberOfPages(); ++p){
			var ret = [];
			var sum = 0;
			var start = $scope.pageSize*p;
			var input = $scope.talks[$scope.currentYear].collections;
			var cnt = 0;
			for(var i  = 0; i < input.length;++i){
				var collection =  angular.copy(input[i]);
				if("seminar" in collection){
					if(!(sum + collection.seminar.length < start || sum > start + $scope.pageSize) && start - sum != collection.seminar.length){
						// console.log(start-sum);
                        var _seminar = {"title":collection.title, "time":collection.time, "date":collection.date, "place":collection.place, "summary": collection.summary}; // copy the original seminar except for seminar key
						if(start - sum > 0){
							_seminar.isMid = true;
						} 
						else{
							_seminar.isMid = false;
						}
						_seminar.seminar = collection.seminar.slice(Math.max(0,start-sum), Math.max(0,start - sum) + $scope.pageSize - cnt);
						cnt += _seminar.seminar.length;
						ret.push(_seminar);
					}
					sum += collection.seminar.length;
				}
				else{
					if(start  <= sum && sum <= start + $scope.pageSize){
						ret.push(collection);
						cnt++;
					}
					sum++;
					// console.log(sum);
				}
				if(cnt >= $scope.pageSize){
					break;
				}
			}
			$scope.sliced.push(ret);
		}
		// console.log($scope.sliced);
	});
	$scope.currentPage = 0;
	$scope.pageSize = 5;
	$scope.orderProp = "date";
	$scope.getControllerScope = function(){
		return $scope;
	};
	$scope.hash = function(val){
		return $location.hash(val);
	}
}]);
